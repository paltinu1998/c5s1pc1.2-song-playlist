package PlayList;

public class Song {
    private String nameOfSong;
    private String duration;

    public Song(String nameOfSong, String duration) {
        this.nameOfSong = nameOfSong;
        this.duration = duration;
    }

    public String getNameOfSong() {
        return nameOfSong;
    }

    public void setNameOfSong(String nameOfSong) {
        this.nameOfSong = nameOfSong;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Song{" +
                "nameOfSong='" + nameOfSong + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}
