package PlayList;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PlaylistCheck {
    List<Song> songs = new ArrayList<>();
    int currentPosition;
    public void addSongs(String name, String duration) {
        songs.add(new Song(name,duration));
    }

    public void addSongAtBeginning(String name,String duration) {
        songs.add(0,new Song(name,duration));
    }

    public String goBack() {
        if(currentPosition == 0) {
            return "It is the 1st song";
        }
        return "Current Song "+songs.get(--currentPosition).toString();
    }

    public String goNext() {
        if(currentPosition == songs.size()-1) {
            return "it is the last song";
        }
        return "Current Song "+songs.get(++currentPosition).toString();
    }

    public String playTheSong(int pos) {
        if(pos > songs.size()) {
            return "There is no song at "+pos;
        }
        currentPosition = pos-1;
        return "Current Song "+songs.get(currentPosition).toString();
    }

    public boolean deleteSong(String name) {
        int i=0;
        for (Song song: songs) {
            if(song.getNameOfSong().equalsIgnoreCase(name) && currentPosition != i) {
                songs.remove(song);
                return true;
            }
        }
        return false;
    }

    public void displayAllSongs() {
        for (Song song: songs) {
            System.out.println(song.toString());
        }
    }

    public static void main(String[] args) {
        PlaylistCheck list = new PlaylistCheck();
        //add songs sequencetially
        list.addSongs("Naina","4:36");
        list.addSongs("Chaska","4:45");
        list.addSongs("Aafreen","5:32");
        list.addSongs("Kun Faaya Kun","7:30");
        list.addSongs("Adhura Lafz","4:23");

        Scanner sc =  new Scanner(System.in);
        boolean flag = true;
        do {
            System.out.println("1.) Add a song at beginning");
            System.out.println("2.) Display number of songs in playlist");
            System.out.println("3.) Go back");
            System.out.println("4.) Go next");
            System.out.println("5.) Go at any position");
            System.out.println("6.) Delete a song");
            System.out.println("7.) Display all songs");
            System.out.println("8.) Exit");
            System.out.println("Enter your choice");
            int choice = sc.nextInt();
            sc.nextLine();
            switch(choice) {
                case 1:
                    System.out.println("Enter the name of song");
                    String name = sc.nextLine();
                    System.out.println("Enter the duration of song");
                    String duration = sc.next();
                    list.addSongAtBeginning(name,duration);
                    System.out.println("Song is added");
                    break;
                case 2:
                    System.out.println("Number of songs in playlist is "+list.songs.size());
                    break;
                case 3:
                    System.out.println(list.goBack());
                    break;
                case 4:
                    System.out.println(list.goNext());
                    break;
                case 5:
                    System.out.println("Enter the number of song");
                    int pos = sc.nextInt();
                    sc.nextLine();
                    System.out.println(list.playTheSong(pos));
                    break;
                case 6:
                    System.out.println("Enter the name of song");
                    name = sc.nextLine();
                    boolean isPresent = list.deleteSong(name);
                    if(isPresent)
                        System.out.println("Song is deleted ");
                    else
                        System.out.println("Song is not present or it is playing");
                    break;
                case 7:
                    list.displayAllSongs();
                    break;
                case 8:
                    flag = false;
                    break;
                default:
                    System.out.println("Enter the right option");
            }
            System.out.println("\n");
        }while(flag);

        System.out.println("Thank you!!!");

    }
}
